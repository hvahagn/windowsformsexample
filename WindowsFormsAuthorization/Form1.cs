﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAuthorization
{
    public partial class Form1 : Form
    {
        bool vis = true;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (vis)
            {
                textBox2.UseSystemPasswordChar = false;
                vis = false;

            }
            else
            {

                textBox2.UseSystemPasswordChar = true;
                vis = true;
            }
            
        }

        private void txtSignin_Click(object sender, EventArgs e)
        {
            if(textBox1.Text =="admin" && textBox2.Text=="1234")
            {
                Form Form2 = new LoginPage();
                Form2.Show();
                this.Hide();
            }
           else
            {
                MessageBox.Show("Incorrect password or login");
            }
        }
    }
}
